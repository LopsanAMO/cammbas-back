from django.urls import path, include
from cambas.users.api.v1 import urls as userUrlsV1


urlpatterns = [
    path('v1/', include(userUrlsV1)),
]
from rest_framework import viewsets, mixins
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.authtoken.models import Token
from cambas.users.models import User
from cambas.users.permissions import IsUserOrReadOnly
from .serializers import CreateUserSerializer, UserSerializer, SocialMediaSerializer, LoginSerializer
from cambas.users.decorators import register_by_access_token
from cambas.users.utils import reset_token


class UserViewSet(mixins.RetrieveModelMixin,
                  mixins.UpdateModelMixin,
                  viewsets.GenericViewSet):
    """
    Updates and retrieves user accounts
    """
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = (IsUserOrReadOnly,)

    def retrieve(self, request, *args, **kwargs):
        serializer = UserSerializer(request.user)
        return Response(status=200, data=serializer.data)

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        instance = User.objects.get(id=request.user.id)
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        return Response(serializer.data)


class UserCreateViewSet(mixins.CreateModelMixin,
                        viewsets.GenericViewSet):
    """
    Creates user accounts
    """
    queryset = User.objects.all()
    serializer_class = CreateUserSerializer
    permission_classes = (AllowAny,)


class LoginViewSet(mixins.CreateModelMixin, viewsets.GenericViewSet):
    """
    Login Clients
    """
    serializer_class = LoginSerializer
    permission_classes = (AllowAny,)

    def create(self, request, *args, **kwargs):
        serializer = LoginSerializer(data=request.data)
        try:
            if serializer.is_valid():
                user = serializer.user
                token, created = Token.objects.get_or_create(user=user)
                return Response(status=200, data={'token': token.key, 'id': str(user.id)})
            else:
                return Response(data=serializer.errors, status=400)
        except Exception as e:
            return Response(status=400, data=e.args[0])


class FacebookView(mixins.CreateModelMixin,
                   viewsets.GenericViewSet):
    """View to authenticate users through Facebook."""

    permission_classes = (AllowAny,)
    serializer_class = SocialMediaSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            try:
                user = register_by_access_token(request, request.data.get('backend'))
            except Exception as e:
                return Response(status=400, data={'detail': 'No se pudo autentificar, token de autentificacion invalido.'})
            if user:
                token = reset_token(user)
                return Response({
                    'token': token,
                    'f_id': user.social_user.uid,
                })
        else:
            return Response(status=400, data=serializer.errors)

from django.urls import path, re_path, include, reverse_lazy
from rest_framework.routers import DefaultRouter
from cambas.users.api.v1.views import UserViewSet, UserCreateViewSet, LoginViewSet, FacebookView

router = DefaultRouter()
router.register(r'facebook', FacebookView, base_name='facebook')
router.register(r'users/login', LoginViewSet, base_name='login')
router.register(r'users/user', UserViewSet)
router.register(r'users', UserCreateViewSet)

urlpatterns = [
    path('', include(router.urls)),
]
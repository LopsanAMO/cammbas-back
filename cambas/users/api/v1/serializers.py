from rest_framework import serializers
from django.contrib.auth import authenticate
from cambas.users.models import User
from social_django.models import UserSocialAuth


class UserSerializer(serializers.ModelSerializer):
    f_id = serializers.SerializerMethodField()
    f_token = serializers.SerializerMethodField()

    class Meta:
        model = User
        fields = ('id', 'username', 'first_name', 'last_name', 'email', 'f_id', 'f_token')
        read_only_fields = ('username', )

    def get_f_id(self, obj):
        user = UserSocialAuth.objects.filter(user_id=obj.id)
        if user:
            holo = user[0].tokens
            return user[0].uid
        else:
            return ''

    def get_f_token(self, obj):
        user = UserSocialAuth.objects.filter(user_id=obj.id)
        if user:
            return user[0].tokens
        else:
            return ''


class CreateUserSerializer(serializers.ModelSerializer):

    def create(self, validated_data):
        user = User.objects.create_user(**validated_data)
        return user

    class Meta:
        model = User
        fields = ('id', 'username', 'password', 'first_name', 'last_name', 'email', 'auth_token',)
        read_only_fields = ('auth_token',)
        extra_kwargs = {
            'password': {'write_only': True},
            'first_name': {'write_only': True},
            'last_name': {'write_only': True},
            'email': {'write_only': True},
            'username':  {'write_only': True}
        }


class SocialMediaSerializer(serializers.Serializer):
    backend = serializers.CharField()
    access_token = serializers.CharField()


class LoginSerializer(serializers.Serializer):
    email = serializers.CharField()
    password = serializers.CharField()

    def validate(self, attrs):
        email = attrs.get('email')
        password = attrs.get('password')
        user = User.objects.filter(email=email)
        if user:
            user = authenticate(username=user[0].username, password=password)
            if user is not None:
                self.user = user
            else:
                raise serializers.ValidationError('Contraseña o email incorrectos')
        else:
            raise serializers.ValidationError('Contraseña o email incorrectos')
        return attrs
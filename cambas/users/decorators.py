
from social_django.utils import psa


@psa('social:complete')
def register_by_access_token(request, backend):
    token = request.data.get('access_token')
    user = request.backend.do_auth(token)
    if user:
        return user
    else:
        return None
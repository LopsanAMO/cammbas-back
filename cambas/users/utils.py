from rest_framework.authtoken.models import Token


def reset_token(user):
    token = Token.objects.filter(user=user)
    token = [t.delete() for t in token]
    token = Token.objects.create(user=user)
    return token.key